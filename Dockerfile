# Includes what you need to run a version 5.x symfony project.
# Based on the php-fpm-alpine. 
FROM php:7.2-fpm-alpine
# https://hub.docker.com/_/php

# If you are having difficulty figuring out which Debian or Alpine packages need to be installed before docker-php-ext-install, then have a look at the install-php-extensions project. This script builds upon the docker-php-ext-* scripts and simplifies the installation of PHP extensions by automatically adding and removing Debian (apt) and Alpine (apk) packages. For example, to install the GD extension you simply have to run install-php-extensions gd. This tool is contributed by community members and is not included in the images, please refer to their Git repository for installation, usage, and issues.

# https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions gd intl opcache pdo_pgsql pgsql pdo_mysql zip

RUN apk add --no-cache nginx

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

COPY ./php/my_php.ini "$PHP_INI_DIR/conf.d/"

RUN mkdir /www && mkdir /www/public
COPY ./index.php /www/public
#CMD ["nginx", "-g", "daemon off;"]
RUN mkdir -p /run/nginx
CMD php-fpm -D && nginx -g "daemon off;"

WORKDIR /www
